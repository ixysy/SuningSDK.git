<?php
/**
 * User: MaxZhang
 * Email:q373884384@gmail.com
 * Date: 2019/6/10 0010
 * Time: 20:38
 */

namespace MaxZhang\SuningSdk\Request\union;


use MaxZhang\SuningSdk\SuningRequest;
use MaxZhang\SuningSdk\Util\RequestCheckUtil;

/**
 * 单笔查询联盟商品信息
 * Class UnionInfomationGetRequest
 * @package MaxZhang\SuningSdk\Request\union
 */
class UnionInfomationGetRequest extends SuningRequest
{

    /**
     * 商品ID。
     */
    private $goodsCode;

    public function getGoodsCode() {
        return $this->goodsCode;
    }

    public function setGoodsCode($goodsCode) {
        $this->goodsCode = $goodsCode;
        $this->apiParams["goodsCode"] = $goodsCode;
    }

    /**
     * 根据请求方式，生成相应请求报文
     *
     * @param
     *            type 请求方式(json或xml)
     */
    public function getApiParams()
    {
        return $this->apiParams;
    }

    /**
     * 获取接口方法名称
     */
    public function getApiMethodName()
    {
        return 'suning.netalliance.unioninfomation.get';
    }

    /**
     * 数据校验
     */
    public function check()
    {
        //非空校验
        RequestCheckUtil::checkNotNull($this->goodsCode, 'goodsCode');
    }

    public function getBizName()
    {
        return "getUnionInfomation";
    }

}