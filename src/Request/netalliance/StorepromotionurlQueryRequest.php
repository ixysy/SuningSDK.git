<?php
/**
 * User: MaxZhang
 * Email:q373884384@gmail.com
 * Date: 2019/6/10 0010
 * Time: 23:22
 */

namespace MaxZhang\SuningSdk\Request\netalliance;


use MaxZhang\SuningSdk\SuningRequest;

/**
 * Class StorepromotionurlQueryRequest 获取商品或店铺推广链接接口
 * @package MaxZhang\SuningSdk\Request\netalliance
 */
class StorepromotionurlQueryRequest extends SuningRequest
{
    /**
     *
     */
    private $adBookId;

    /**
     *
     */
    private $commCode;

    /**
     *
     */
    private $mertCode;

    /**
     *
     */
    private $urlType;


    public function getAdBookId()
    {
        return $this->adBookId;
    }

    /**
     * @param $adBookId 广告位
     */
    public function setAdBookId($adBookId)
    {
        $this->adBookId = $adBookId;
        $this->apiParams["adBookId"] = $adBookId;
    }

    public function getCommCode()
    {
        return $this->commCode;
    }

    /**
     * @param $commCode 商品编码 如果商品编码不为空，则生成商品推广链接，否则生成店铺推广链接
     */
    public function setCommCode($commCode)
    {
        $this->commCode = $commCode;
        $this->apiParams["commCode"] = $commCode;
    }

    public function getMertCode()
    {
        return $this->mertCode;
    }

    /**
     * @param $mertCode 商家编码 10位数字
     */
    public function setMertCode($mertCode)
    {
        $this->mertCode = $mertCode;
        $this->apiParams["mertCode"] = $mertCode;
    }

    public function getUrlType()
    {
        return $this->urlType;
    }

    /**
     * @param $urlType url类型 1:长链接 2:短链接
     */
    public function setUrlType($urlType)
    {
        $this->urlType = $urlType;
        $this->apiParams["urlType"] = $urlType;
    }

    public function getApiMethodName()
    {
        return 'suning.netalliance.storepromotionurl.query';
    }

    public function getApiParams()
    {
        return $this->apiParams;
    }

    public function check()
    {
        //非空校验
        RequestCheckUtil::checkNotNull($this->adBookId, 'adBookId');
        RequestCheckUtil::checkNotNull($this->mertCode, 'mertCode');
    }

    public function getBizName()
    {
        return "queryStorepromotionurl";
    }
}