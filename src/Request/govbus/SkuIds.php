<?php
/**
 * User:  MaxZhang
 * Email: q37388438@gmail.com
 * Date: 2019/06/10
 * Time: 19:39
 */

namespace MaxZhang\SuningSdk\Request\Govbus;


class SkuIds {

    private $apiParams = array();

    private $skuId;
    private $num;
    private $piece;

    public function getNum() {
        return $this->num;
    }

    public function setNum($num) {
        $this->num = $num;
        $this->apiParams["num"] = $num;
    }

    public function getSkuId() {
        return $this->skuId;
    }

    public function setSkuId($skuId) {
        $this->skuId = $skuId;
        $this->apiParams["skuId"] = $skuId;
    }

    public function getPiece() {
        return $this->piece;
    }

    public function setPiece($piece) {
        $this->piece = $piece;
        $this->apiParams["piece"] = $piece;
    }

    public function getApiParams(){
        return $this->apiParams;
    }

}
